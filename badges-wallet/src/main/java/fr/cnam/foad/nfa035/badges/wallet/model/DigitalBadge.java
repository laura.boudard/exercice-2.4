package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge implements Comparable<DigitalBadgeMetadata>{

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;


    /**
     * Constructeur
     *  @param metadata
     * @param begin
     * @param end
     * @param serial
     */
    public DigitalBadge(String meta, Date begin, Date end, DigitalBadgeMetadata metadata, String serial) {
        meta = metadata.toString();
        this.metadata = metadata;
        this.begin = begin;
        this.end = end;
        this.serial = serial;
    }

    /**
     *
     * @param metadata
     * @param badge
     * @param begin
     * @param end
     * @param serial
     */
    public DigitalBadge(String metadata, File badge, Date begin, Date end, String serial) {
        metadata = metadata.toString();
        this.badge = badge;
        this.begin = begin;
        this.end = end;
        this.serial = serial;
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {this.badge = badge;}

    /**
     * Getter de la date d'entrée de l'image
     * @return la date d'entrée
     */
    public Date getBegin() {return begin; }

    /**
     * Setter de la date d'entrée de l'image
     * @param begin
     */
    public void setBegin(Date begin) {this.begin = begin;}

    /**
     * Getter de péremption
     * @return la date de péremption
     */
    public Date getEnd() {return end;}

    /**
     * Setter de la date la date de péremption
     * @param end
     */
    public void setEnd(Date end) {this.end = end;}

    /**
     * Getter du numéro de série
     * @return le code série de l'image
     */
    public String getSerial() {return serial;}

    /**
     * Setter du numéro de série
     * @param serial
     */
    public void setSerial(String serial) {this.serial = serial;}

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge) && begin.equals(that.begin) && end.equals(that.end) && serial.equals(that.serial);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge, begin, end, serial);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ", date d'obtention=" + begin +
                ", date de péremption=" + end +
                ", numéro de série=" + serial +
                '}';
    }


    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>The implementor must ensure {@link Integer#signum
     * signum}{@code (x.compareTo(y)) == -signum(y.compareTo(x))} for
     * all {@code x} and {@code y}.  (This implies that {@code
     * x.compareTo(y)} must throw an exception if and only if {@code
     * y.compareTo(x)} throws an exception.)
     *
     * <p>The implementor must also ensure that the relation is transitive:
     * {@code (x.compareTo(y) > 0 && y.compareTo(z) > 0)} implies
     * {@code x.compareTo(z) > 0}.
     *
     * <p>Finally, the implementor must ensure that {@code
     * x.compareTo(y)==0} implies that {@code signum(x.compareTo(z))
     * == signum(y.compareTo(z))}, for all {@code z}.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     * @apiNote It is strongly recommended, but <i>not</i> strictly required that
     * {@code (x.compareTo(y)==0) == (x.equals(y))}.  Generally speaking, any
     * class that implements the {@code Comparable} interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     */
    @Override
    public int compareTo(DigitalBadgeMetadata o) {
        DigitalBadgeMetadata a = o;
         if (a.getBadgeId() == o.getBadgeId()) return 0;
         if (a.getBadgeId()>o.getBadgeId()) return 1;
         else  return -1;
    }
}
